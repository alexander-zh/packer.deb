# Packer.deb #

## What is it

[Packer](https://www.packer.io/) is a tool for creating machine and container images for multiple platforms from a single source configuration.

This repo contains script to build deb-package from official packer binary package. Resulting deb-file is included. Tested with Ubuntu 14.04 and Ubuntu 16.04. Should work with any Debian-based Linux distribution.


## How to install packer


```
#!shell

git clone https://bitbucket.org/alexander-zh/packer.deb
sudo dpkg -i packer.deb/packer_0.10.1_amd64.deb
```

## What is not done
* man packer
* PPA repository