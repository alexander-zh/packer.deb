#!/bin/bash

VERSION="$(./usr/bin/packer -v)";
fpm \
    -s dir \
    -t deb \
    --name packer \
    --version "$VERSION" \
     usr/ \
;
